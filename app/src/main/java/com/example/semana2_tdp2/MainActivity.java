package com.example.semana2_tdp2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.transition.Fade;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<itemModel> arrayList;
    public static final int PERMISSION_WRITE = 0;
    CustomAdapter adapter;
    Button btn_gallery;
   // Animation a = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.push_left_in);
    RecyclerView.ItemAnimator hola = new RecyclerView.ItemAnimator() {
        @Override
        public boolean animateDisappearance(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull ItemHolderInfo itemHolderInfo, @Nullable ItemHolderInfo itemHolderInfo1) {
            return false;
        }

        @Override
        public boolean animateAppearance(@NonNull RecyclerView.ViewHolder viewHolder, @Nullable ItemHolderInfo itemHolderInfo, @NonNull ItemHolderInfo itemHolderInfo1) {
            viewHolder.itemView.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_anim));
            return false;
        }

        @Override
        public boolean animatePersistence(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull ItemHolderInfo itemHolderInfo, @NonNull ItemHolderInfo itemHolderInfo1) {
            return false;
        }

        @Override
        public boolean animateChange(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1, @NonNull ItemHolderInfo itemHolderInfo, @NonNull ItemHolderInfo itemHolderInfo1) {
            return false;
        }

        @Override
        public void runPendingAnimations() {

        }

        @Override
        public void endAnimation(@NonNull RecyclerView.ViewHolder viewHolder) {

        }

        @Override
        public void endAnimations() {

        }

        @Override
        public boolean isRunning() {
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(linearLayoutManager.HORIZONTAL);
        btn_gallery= findViewById(R.id.add_button);

        checkPermission();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(hola);

        arrayList = new ArrayList<>();
        new fetchData().execute();


    }
    public void waiting(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                waiting();
            }
        }, 2000);
    }
    public void Add(View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        //añadir 1
        startActivityForResult(Intent.createChooser(intent,"Pick Image"),1);
    }

    public void Add2(View view) throws InterruptedException {
        String stringer ="https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg" ;
        itemModel model = new itemModel();
        model.setImage(stringer);
        recyclerView.getLayoutManager().scrollToPosition(0);
        arrayList.add(0,model);
        adapter.notifyItemInserted(0);
        arrayList.add(0,model);
        adapter.notifyItemInserted(0);

    }
    public void Add3(View view) throws InterruptedException {
        String stringer ="https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg" ;
        itemModel model = new itemModel();
        model.setImage(stringer);
        recyclerView.getLayoutManager().scrollToPosition(0);
        arrayList.add(0,model);
        adapter.notifyItemInserted(0);
        arrayList.add(0,model);
        adapter.notifyItemInserted(0);
        arrayList.add(0,model);
        adapter.notifyItemInserted(0);
        recyclerView.getLayoutManager().scrollToPosition(0);
    }

    @Override
    protected void onActivityResult(int requestedCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestedCode,resultCode,data);
        //agregar 1
        if(resultCode==RESULT_OK && requestedCode ==1){
            Uri path = data.getData();
            String stringer = path.toString();
            itemModel model = new itemModel();
            model.setImage(stringer);
            arrayList.add(model);

            adapter.notifyItemInserted(arrayList.size() -1);
        }

    }

    public class fetchData extends AsyncTask<String, String, String> {

        @Override
        public void onPreExecute() {
            super .onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            arrayList.clear();
            String result = null;
            try {
                URL url = new URL("https://reqres.in/api/users?page=2");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.connect();

                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());
                    BufferedReader reader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String temp;

                    while ((temp = reader.readLine()) != null) {
                        stringBuilder.append(temp);
                    }
                    result = stringBuilder.toString();
                }else  {
                    result = "error";
                }

            } catch (Exception  e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        public void onPostExecute(String s) {
            super .onPostExecute(s);
            try {
                JSONObject object = new JSONObject(s);
                JSONArray array = object.getJSONArray("data");

                for (int i = 0; i < array.length(); i++) {

                    JSONObject jsonObject = array.getJSONObject(i);
                    String image = jsonObject.getString("avatar");

                    itemModel model = new itemModel();
                    model.setImage(image);
                    arrayList.add(model);
                }
            } catch (JSONException  e) {
                e.printStackTrace();
            }

            adapter = new CustomAdapter(getApplicationContext(), arrayList, MainActivity.this);

            new ItemTouchHelper(itemTouchHelperCallBack).attachToRecyclerView(recyclerView);
            recyclerView.setAdapter(adapter);

        }
    }
    //runtime storage permission
    public boolean checkPermission() {
        int READ_EXTERNAL_PERMISSION = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if((READ_EXTERNAL_PERMISSION != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_WRITE);
            return false;
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==PERMISSION_WRITE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //do somethings
        }
    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallBack = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.UP) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            arrayList.remove(viewHolder.getAdapterPosition());
            adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
        }
    };
}